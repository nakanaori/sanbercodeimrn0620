import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View} from 'react-native';
// import Login from './Tugas/Tugas13/LoginScreen';
import About from './Tugas/Tugas13/AboutScreen';
// import Main from "./Tugas/Tugas14/components/Main"
// import Skill from "./Tugas/Tugas14/SkillScreen"
// import Index from "./Tugas/Tugas15/index";
// import Index from "./Tugas/TugasNavigation/index";
import Index from "./Tugas/Quiz3/index"

export default class App extends Component{
    render(){
        return (
            <View style = {styles.container}>
                {/* <Login /> */}
                {/* <About /> */}
                {/* <Main /> */}
                {/* <Skill /> */}
                <Index />
                
            </View>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,        
  }
});
