import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, FlatList} from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import SkillList from "./components/skillList";
import data from "./skillData.json"

export default class Skill extends Component{
    render(){
        return (
            <View style = {styles.container}>
                    <View style = {styles.logoHeader}>
                        <Image source = {require("./images/logo.png")} style = {styles.logo} />
                    </View>
                    <View style = {styles.user}>
                        <MaterialCommunityIcons name="account-circle" size={40} color="#3EC6FF" />
                        <View style = {styles.greeting}>
                            <Text>Hai,</Text>
                            <Text style = {{color : "#003366"}}>Varianto Nakanaori</Text>
                        </View>
                    </View>
                    <View style = {{flex : 1}}>
                            <Text style = {styles.title}>Skill</Text>
                            <View style = {{borderBottomColor: '#003366',borderBottomWidth: 2,marginHorizontal : 15}} />
                        <View style = {styles.skillTabs}>                           
                            <TouchableOpacity style = {styles.tabItems}>
                                <Text style = {styles.tabText}>Library/Framework</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style = {styles.tabItems}>
                                <Text style = {styles.tabText}>Bahasa Pemogramman</Text>
                            </TouchableOpacity>     
                            <TouchableOpacity style = {styles.tabItems}>
                                <Text style = {styles.tabText}>Teknologi</Text>
                            </TouchableOpacity>                     
                        </View>
                        <View style = {styles.skillLists}>
                            <FlatList data = {data.items}
                            renderItem = {(skill) => <SkillList skill = {skill.item} />}
                            keyExtractor = {(item) => item.id.toString()}
                            />
                        </View>
                    </View>
                
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,        
    },
    logoHeader: {
        alignItems : "flex-end",
        height : 80
    },
    logo : {
        resizeMode : "contain",
        height : 110,
        width : 150,
    },
    user : {
        flexDirection : "row",
        paddingBottom : 3,
        marginHorizontal : 10
    },
    greeting : {
        flexDirection : "column"
    },
    title : {
        fontSize : 45,
        paddingHorizontal : 15,
        color : "#003366",
        fontWeight : "bold",
    },
    skillTabs : {
        flexDirection : "row",
        justifyContent : "space-evenly",

    },
    tabItems : {
        marginVertical : 8,
        padding : 5,
        backgroundColor : "#B4E9FF",
        borderRadius : 14
    },
    tabText : {
        fontSize : 13,
        color : "#003366",
    },
    skillLists : {
        flex : 1
    }
  });