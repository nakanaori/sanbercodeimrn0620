import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TextInput, Button, ImageBackground, TouchableOpacity} from 'react-native';


export default class Login extends Component{
    render(){
        return (
            <View style = {styles.container}>
                <StatusBar hidden />
                <View style = {styles.title}>
                    <Image source = {require('./images/logo.png')} style = {styles.logo}/>
                    <Text style = {styles.loginTitle}>Login</Text>
                </View>
                <View style = {styles.body}>
                            <Text style = {styles.inputInfo}>Username/Email</Text>
                        <View style = {styles.InputForm}>
                            <TextInput style = {styles.inputField} placeholder = "Username/Email" />
                        </View>
                        <Text style = {styles.inputInfo}>Password</Text>
                        <View style = {styles.InputForm}>
                            <TextInput style = {styles.inputField} placeholder = "Password"/>
                        </View>
                        <View style = {styles.button}>
                            <Button style = {styles.tabItem} title = "Masuk"  onPress={() => this.props.navigation.navigate("About")} />
                            <Text style = {styles.seperate}>Atau</Text>
                            <TouchableOpacity style = {styles.tabItem2}>
                                <Text style = {styles.tabText}>Daftar</Text>
                            </TouchableOpacity>
                        </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    title : {
        backgroundColor : "#EFEFEF",
        justifyContent : "center",
        alignItems : "center",
        padding : 5
    },
    logo : {
        margin : 20,
       
    },
    loginTitle : {
        fontSize : 25,
        marginBottom : 20
    },
    userForm :{
        marginBottom : 10,
        alignItems : "center",
        textAlign : "left"
    },
    InputForm : {
        alignItems : "center",
    },
    inputInfo : {
        marginBottom : -10,
        paddingLeft : 25,
        fontSize : 13,
        textAlign : "left",
    },
    inputField : {
        borderWidth : 1,
        borderColor : '#777',
        padding : 8,
        margin : 10,
        width : 300,
    },
    button : {
        alignItems : "center",
        justifyContent : "center",
        paddingTop : 14
    },
    tabItem : {
        backgroundColor : "#3EC6FF",
        height : 40,
        width : 140,
        borderRadius : 25,
    },
    tabText : {
        alignSelf : "center",
        fontSize : 20,
        fontWeight : "bold",
        color : 'white',
        paddingTop : 6
    },
    tabItem2 : {
        backgroundColor : "#003366",
        height : 40,
        width : 140,
        borderRadius : 25,
    },
    seperate : {
        padding : 8,
        fontSize : 17,
        color : "#3EC6FF"
    }
  });
  