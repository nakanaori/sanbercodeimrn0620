import React from "react";
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createDrawerNavigator} from '@react-navigation/drawer';
import Login from "./LoginScreen";
import Skill from "./SkillScreen";
import About from "./AboutScreen";
import Project from "./ProjectScreen";
import Add from "./AddScreen";
import { Root } from "./Screen";

const rootStack = createStackNavigator();
const Drawer = createDrawerNavigator();
const Tabs = createBottomTabNavigator();
const loginStack = createStackNavigator();
const aboutStack = createDrawerNavigator();

const TabsScreen = () => (
    <Tabs.Navigator>
        <Tabs.Screen name = "Project" component = {Project} />
        <Tabs.Screen name = "Add" component = {Add} />
        <Tabs.Screen name = "Skill" component = {Skill} />
    </Tabs.Navigator>
)

const DrawerScreen = () => (
    <Drawer.Navigator>
        <Drawer.Screen name = "DrawerNavigator" component = {TabsScreen}/>
        <Drawer.Screen name = "About" component = {About} />
    </Drawer.Navigator>
)

const rootScreen = () => (
    <rootStack.Navigator>
        <rootStack.Screen name = "root" component = {Root} />
        <rootStack.Screen name = "Login" component = {Login}/>
        <rootStack.Screen name = "DrawerScreen" component = {DrawerScreen}/>
    </rootStack.Navigator>
)

const LoginScreen = () => (
    <loginStack.Navigator>
        <loginStack.Screen name = "Login" component = {Login}/>
        <loginStack.Screen name = "About" component = {AboutScreen} options = {{title : "About"}}/>
    </loginStack.Navigator>
)

const AboutScreen = () => (
    <aboutStack.Navigator>
        <aboutStack.Screen name = "About" component = {About} options = {{title : "About"}} />
        <aboutStack.Screen name = "DrawerScreen" component = {DrawerScreen}/>
    </aboutStack.Navigator>
)

export default () => (
  <NavigationContainer>
    <rootStack.Navigator>
        <rootStack.Screen name = "root" component = {Root} />
        <rootStack.Screen name = "Login" component = {LoginScreen}/>
    </rootStack.Navigator>
  </NavigationContainer>
);
