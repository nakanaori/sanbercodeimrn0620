import React from "react";
import { View, Text, StyleSheet, Button } from "react-native";
import Login from "./LoginScreen";
import Skill from "./SkillScreen";
import About from "./AboutScreen";
// import { AuthContext } from "./context";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  button: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    marginVertical: 10,
    borderRadius: 5
  }
});

const ScreenContainer = ({ children }) => (
  <View style={styles.container}>{children}</View>
);

export const Root = ({navigation}) => (
  <ScreenContainer>
    <Text>Root Screen</Text>
    <Button title="Login" onPress={() => navigation.navigate("Login")} />
  </ScreenContainer>
);

