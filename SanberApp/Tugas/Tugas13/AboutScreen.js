import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View} from 'react-native';
import { MaterialIcons, MaterialCommunityIcons } from '@expo/vector-icons';


export default class About extends Component{
    render(){
        return (
            <View style = {styles.container}>
                <StatusBar hidden/>
                <View style = {styles.title}>
                    <Text style = {styles.titleText}>Tentang Saya</Text>
                </View>
                <View style = {styles.accPict}>
                    <MaterialIcons name="account-circle" size={120} color="#EFEFEF" />
                </View>
                <View style = {styles.personalInfo}>
                    <View style = {styles.bioData}>
                        <Text style = {styles.nameText}>Varianto Nakanaori</Text>
                    </View>
                    <View style = {styles.bioData}>
                        <Text style = {styles.jobText}>React Native Developer</Text>
                    </View>
                </View>
                <View style = {styles.box}>
                    <Text style = {{marginLeft :10, color : "#003366"}}>Portofolio</Text>
                    <View style = {{borderBottomColor: '#003366',borderBottomWidth: 1,marginHorizontal : 10}} />
                    <View style = {styles.tabList}>
                        <View style = {styles.tabItem}>
                            <MaterialCommunityIcons name="gitlab" size={70} color="#3EC6FF" />
                            <Text style = {styles.idText}>@nakanaori</Text>
                        </View>
                        <View style = {styles.tabItem}>
                            <MaterialCommunityIcons name="github-circle" size={70} color="#3EC6FF" />
                            <Text style = {styles.idText}>@nakanaori</Text>
                        </View>
                    </View>
                </View>
                <View style = {styles.box}>
                    <Text style = {{marginLeft :10 , color : "#003366"}}>Hubungi Saya</Text>
                    <View style = {{borderBottomColor: '#003366',borderBottomWidth: 1,marginHorizontal : 10}} />
                    <View style = {styles.tabContact}>
                        <View style = {styles.tabListContact}>
                            <MaterialCommunityIcons name="facebook-box" size={24} color="#3EC6FF" style = {{marginRight : 10}} />
                            <Text style = {styles.contactText}>Varianto Nakanaori</Text>
                        </View>
                        <View style = {styles.tabListContact}>
                            <MaterialCommunityIcons name="instagram" size={24} color="#3EC6FF" style = {{marginRight : 10}}/>
                            <Text style = {styles.contactText}>@vnakanaori</Text>
                        </View>
                        <View style = {styles.tabListContact}>
                            <MaterialCommunityIcons name="twitter-circle" size={24} color="#3EC6FF" style = {{marginRight : 10}}/>
                            <Text style = {styles.contactText}>vnakanaori</Text>
                        </View>
                        
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,        
    },
    title :{
        alignItems : "center",
        justifyContent : "center",
        padding : 11
    },
    titleText : {
        fontSize : 35,
        fontWeight : "bold",
        color : "#003366",
    },
    accPict : {
        alignItems : "center",
        justifyContent : "center"
    },
    personalInfo : {
        alignItems : "center",
        justifyContent : "center",
    },
    bioData : {
        backgroundColor : "white",
        textAlign : "justify",
        padding : 2
    },
    nameText : {
        fontSize : 20,
        color : "#003366"
    },
    jobText : {
        fontSize : 16,
        color : "#3EC6FF"
    },
    box : {
        backgroundColor : "#EFEFEF",
        margin : 15,
    },
   tabList:{
        flexDirection : "row",
        justifyContent : "space-evenly"
   },
   tabItem :{
        alignItems : "center",
        justifyContent : "center"
   },
   idText : {
       fontWeight : "bold",
       color : "#003366"
   },
   tabListContact :{
       flexDirection : "row",
       margin : 10,
       alignContent : "center",
       paddingLeft : 85
   },
   contactText : {
       color : "#003366",
       fontSize : 17
   }
  });
  