import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';

export default class SkillList extends Component{
    render(){
        let skill = this.props.skill;
        return (
            <View style = {styles.container}>
                <View style = {styles.logoPict}>
                    <MaterialCommunityIcons name = {skill.iconName} size={60} color="#003366"/>
                </View>
                <View style = {styles.skillInfo}>
                    <Text style = {styles.name}>{skill.skillName}</Text>
                    <Text style = {styles.category}>{skill.categoryName}</Text>
                    <Text style = {styles.percent}>{skill.percentageProgress}</Text>
                </View>
                <View style = {styles.arrowContainer}>
                    <TouchableOpacity style = {styles.arrow}>
                        <MaterialCommunityIcons name="chevron-right" size={60} color="#003366" />
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container : {
        backgroundColor : "#B4E9FF",
        margin : 10,
        borderRadius : 16,
        flexDirection : "row",
        justifyContent : "space-between",
        alignItems : "center"
    },
    arrow : {
        alignSelf : "flex-end",
        resizeMode : "stretch"
    },
    logoPict : {
        padding : 10,
    },
    name : {
        fontSize : 25,
        color : "#003366",
    },
    category : {
        fontSize : 16,
        color : "#3EC6FF",
    },
    percent : {
        alignSelf : "flex-end",
        fontSize : 50,
        color : "white"
    }
});