class Animal {
    // Code class di sini
    constructor(name){
        this._animalName = name;
        this._legs = 4;
        this._coldBlooded = false;
    }
    set name(animalName){
        this._animalName = animalName;
    }
    get name(){
        return this._animalName;
    }
    set legs(animalLegs){
        this._legs = animalLegs;
    }
    get legs(){
        return this._legs;
    }
    set cold_blooded(coldBlooded){
        this._coldBlooded = coldBlooded;
    }
    get cold_blooded(){
        return this._coldBlooded;
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

// Code class Ape dan class Frog di sini
class Ape extends Animal{
    constructor(name){
        super(name);
        this._legs = 2;
    }
    yell(){
        console.log("Auooo");
    }
}

class Frog extends Animal{
    constructor(name){
        super(name);
    }
    jump(){
        console.log("hop hop");
    }
}
 
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

class Clock {
    constructor({template}){
        this._template = template;
        this._timer;
    }

    render(){
        var date = new Date();

        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        var output = this._template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);

        console.log(output);
    }
    stop(){
        clearInterval(this._timer);
    }
    start(){
        this.render.bind(this);
        this._timer = setInterval(this.render.bind(this),1000);
    }
}


var clock = new Clock({template: 'h:m:s'});
clock.start();  


