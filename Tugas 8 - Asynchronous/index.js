// di index.js
var readBooks = require('./callback.js')
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
// Tulis code untuk memanggil function readBooks di sini
function startRead(index, times){
    if(index > books.length - 1) return "";
    readBooks(times, books[index], function(){
        return startRead(index+1, times - books[index].timeSpent);
    })
}

startRead(0,10000);