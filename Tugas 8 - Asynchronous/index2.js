var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise
function startRead(index, times){
    if(index > books.length - 1) return "";
    readBooksPromise(times,books[index]).then(function(fullfilled){
        startRead(index + 1, times - books[index].timeSpent);
    })
    .catch(function(error){
        console.log(error);
        return;
    })
}

startRead(0,10000);