console.log("Soal 1");
var count = 2;
var first = true;
var kata = "I love coding";
console.log("LOOPING PERTAMA");
while(count != 0){
    console.log(count + " - " + kata);
    if(first){
        count += 2;
    }else{
        count -= 2;
    }
    if(count == 20){
        console.log(count + " - " + kata);
        console.log("LOOPING KEDUA");
        kata = "I will become a mobile developer";
        first = false;
    }
}

console.log("Soal 2");
console.log("OUTPUT");
for(var i = 1;i<=20;i++){
    if(i % 3 == 0 && i % 2 == 1){
        console.log(i + " - I Love Coding");
    }else if(i % 2 == 1){
        console.log(i + " - Santai");
    }else if(i % 2 == 0){
        console.log(i + " - Berkualitas");
    }
}
console.log("Soal 3");
for(var i = 0;i<4;i++){
    var res = "";
    for(var j = 0;j<8;j++){
        res += "#";
    }
    console.log(res);
}
console.log("Soal 4");
for(var i = 0;i<7;i++){
    var res = "";
    for(var j = 0;j<=i;j++){
        res += "#";
    }
    console.log(res);
}
console.log("Soal 5");
for(var i = 0;i < 8 ;i++){
    var res = "";
    for(var j = 0; j < 8;j++){
        if(i % 2 == 0){
            if(j % 2 == 1){
                res += "#";
            }else{
                res += " ";
            }
        }else if(i % 2 == 1){
            if(j % 2 == 0){
                res += "#";
            }else{
                res += " ";
            }
        }
    }
    console.log(res);
}