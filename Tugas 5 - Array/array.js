function range(startNum, finishNum){
    if(startNum == undefined || finishNum == undefined) return -1;
    var a = [];
    if(startNum < finishNum){
        for(var i = startNum; i <= finishNum;i++){
            a.push(i);
        }
    }else{
        for(var i = startNum; i >= finishNum;i--){
            a.push(i);
        }
    }
    return a; 
}
console.log(range());

function rangeWithStep(startNum, finishNum, step){
    var a = [];
    if(startNum < finishNum){
        for(var i = startNum; i <= finishNum;i+=step){
            a.push(i);
        }
    }else{
        for(var i = startNum; i >= finishNum;i-=step){
            a.push(i);
        }
    }
    if(a.length == 0) return -1;
    return a; 
}
console.log(rangeWithStep(29,2,4));

function sum(startNum, finishNum, step){
    var a = 0;
    if(step == undefined) step = 1;
    if(finishNum == undefined) return 1;
    if(startNum == undefined) return 0;
    if(startNum < finishNum){
        for(var i = startNum; i <= finishNum;i+=step){
            a += i;
        }
    }else{
        for(var i = startNum; i >= finishNum;i-=step){
            a += i;
        }
    }
    return a;
}

console.log(sum(20,10,2));

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

function dataHandling(inputan){
    for(var i = 0;i<inputan.length;i++){
        console.log(`Nomor ID : ${inputan[i][0]}`);
        console.log(`Nama Lengkap : ${inputan[i][1]}`);
        console.log(`TTL : ${inputan[i][2]} ${inputan[i][3]}`);
        console.log(`Hobi : ${inputan[i][4]}`);
        console.log('');
    }
}

dataHandling(input);

function balikKata(stringInput){
    var a = "";
    for(var i = stringInput.length - 1;i>=0;i--){
        a+=stringInput[i];
    }
    return a;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah"))// hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

function dataHandling2(inputan){
    var a = inputan;
    for(var i = 0;i<inputan.length;i++){
        a[i].splice(1,1,`${inputan[i][1]} Elsharawy`);
        a[i].splice(2,1,`Provinsi ${inputan[i][2]}`);
        a[i].splice(4,2,"Pria", "SMA Internasional Metro");
        console.log(a[i]);
        var tanggal = a[i][3].split("/");
        switch(parseInt(tanggal[1])){
            case 1:{
                console.log("Januari");
                break;
            }case 2:{
                console.log("Februari");
                break;
            }case 3:{
                console.log("Maret");
                break;
            }case 4:{
                console.log("April");
                break;
            }case 5:{
                console.log("Mei");
                break;
            }case 6:{
                console.log("Juni");
                break;
            }case 7:{
                console.log("Juli");
                break;
            }case 8:{
                console.log("Agustus");
                break;
            }case 9 :{
                console.log("September");
                break;
            }case 10:{
                console.log("Oktober");
                break;
            }case 11:{
                console.log("November");
                break;
            }case 12:{
                console.log("Desember");
                break;
            }
        }
        var tempTanggal = [...tanggal];
        var tanggalSort = tanggal.sort(function (a,b){ return parseInt(b) - parseInt(a)});
        console.log(tanggalSort);
        var tanggalJoin = tempTanggal.join("-");
        console.log(tanggalJoin);
        var nama15 = a[i][1].slice(0,14);
        console.log(nama15);
    }
}

dataHandling2(input);