function arrayToObject(arr) {
    if(arr.length == 0) {
        console.log('""');
        return;
    }
    var date = new Date();
    var thisYear = date.getFullYear();
    for(var i = 0;i<arr.length;i++){
        var now = {};
        now.firstName = arr[i][0];
        now.lastName = arr[i][1];
        now.gender = arr[i][2];
        now.age = thisYear - arr[i][3];
        if(isNaN(now.age) || now.age <= 0) now.age = "Invalid Birth Year";
        console.log(`${i + 1}. ${now.firstName} ${now.lastName} :`, now);
    }
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""


function shoppingTime(memberId, money) {
    // you can only write your code here!
    if(memberId == undefined || memberId.length == 0) return "Mohon maaf, toko X hanya berlaku untuk member saja";
    if(money < 50000) return "Mohon maaf, uang tidak cukup";
    var barangBelanjaan  = [["Sepatu Stacattu",1500000] ,
                            ["Baju Zoro",500000],
                            ["Baju H&N", 250000],
                            ["Sweater Uniklooh",175000],
                            ["Casing Handphone",50000]];
    var res = {};
    res.memberId = memberId;
    res.money = money;
    res.listPurchased = [];
    for(var i = 0;i<barangBelanjaan.length;i++){
        if(money >= barangBelanjaan[i][1]){
            money -= barangBelanjaan[i][1];
            res.listPurchased.push(barangBelanjaan[i][0]);
        }
    }
    res.changeMoney = money;
    return res;
    
  }
   
  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
    //{ memberId: '1820RzKrnWn08',
    // money: 2475000,
    // listPurchased:
    //  [ 'Sepatu Stacattu',
    //    'Baju Zoro',
    //    'Baju H&N',
    //    'Sweater Uniklooh',
    //    'Casing Handphone' ],
    // changeMoney: 0 }
  console.log(shoppingTime('82Ku8Ma742', 170000));
  //{ memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }
  console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja



  function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var res = [];
    for(var i = 0;i<arrPenumpang.length;i++){
        var temp = {};
        var idxAwal, idxAkhir;
        temp.penumpang = arrPenumpang[i][0];
        temp.naikDari  = arrPenumpang[i][1];
        temp.tujuan = arrPenumpang[i][2];
        for(var j =0;j<rute.length;j++){
            if(rute[j] == arrPenumpang[i][1]) idxAwal = j;
            if(rute[j] == arrPenumpang[i][2]) idxAkhir = j;
        }
        temp.bayar = (idxAkhir - idxAwal) * 2000;
        res.push(temp);
    }
    
    return res;
    //your code here
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
 console.log(naikAngkot([])); //[]