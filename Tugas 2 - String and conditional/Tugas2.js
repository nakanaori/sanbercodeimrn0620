var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';
console.log("Soal 1");
console.log(word + " " + second + " "+ third + " " +fourth + " " + fifth + " " + sixth + " "+ seventh);
console.log("---------------------------------");


var sentence = "I am going to be React Native Developer"; 
var exampleFirstWord = sentence[0] ; 
var secondWord = sentence[2] + sentence[3]  ; 
var thirdWord = sentence[5] + sentence[6] + sentence[7] + sentence[8] + sentence[9]; // lakukan sendiri 
var fourthWord = sentence[11] + sentence[12]; // lakukan sendiri 
var fifthWord = sentence[14] + sentence[15]; // lakukan sendiri 
var sixthWord = sentence[17] + sentence[18] + sentence[19] + sentence[20] + sentence[21];// lakukan sendiri 
var seventhWord =  sentence[23] + sentence[24] + sentence[25] + sentence[26] + sentence[27] + sentence[28]; 
var eighthWord = sentence[30] + sentence[31] + sentence[32] + sentence[33] + sentence[34] + sentence[35] + sentence[36] + sentence[37] + sentence[38];
console.log("Soal 2");
console.log('First Word: ' + exampleFirstWord); 
console.log('Second Word: ' + secondWord); 
console.log('Third Word: ' + thirdWord); 
console.log('Fourth Word: ' + fourthWord); 
console.log('Fifth Word: ' + fifthWord); 
console.log('Sixth Word: ' + sixthWord); 
console.log('Seventh Word: ' + seventhWord); 
console.log('Eighth Word: ' + eighthWord);

console.log("---------------------------------");

var sentence2 = 'wow JavaScript is so cool'; 
var exampleFirstWord2 = sentence2.substring(0, 3); 
var secondWord2 = sentence2.substring(4,14); 
var thirdWord2 = sentence2.substring(15,17); 
var fourthWord2 = sentence2.substring(18,20); 
var fifthWord2 = sentence2.substring(21); 
console.log("Soal 3");
console.log('First Word: ' + exampleFirstWord2); 
console.log('Second Word: ' + secondWord2); 
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2);
console.log("---------------------------------");

var sentence3 = 'wow JavaScript is so cool'; 

var exampleFirstWord3 = sentence3.substring(0, 3); 
var secondWord3 = sentence3.substring(4,14) ; 
var thirdWord3 = sentence3.substring(15,17);  
var fourthWord3 = sentence3.substring(18,20); 
var fifthWord3 = sentence3.substring(21);  

var firstWordLength = exampleFirstWord3.length;
var secondWordLength = secondWord3.length;
var thirdWordLength = thirdWord3.length;
var fourthWordLength = fourthWord3.length;
var fifthWordLength = fifthWord3.length;
// lanjutkan buat variable lagi di bawah ini 
console.log("Soal 4");
console.log('First Word: ' + exampleFirstWord3 + ', with length: ' + firstWordLength); 
console.log('Second Word: ' + secondWord3 + ', with length: ' + secondWordLength); 
console.log('Third Word: ' + thirdWord3 + ', with length: ' + thirdWordLength); 
console.log('Fourth Word: ' + fourthWord3 + ', with length: ' + fourthWordLength); 
console.log('Fifth Word: ' + fifthWord3 + ', with length: ' + fifthWordLength); 
console.log("---------------------------------\n\n");

console.log("Soal B");
console.log("If Else Condition");
var nama = "John";
var peran = "Guard";

if(nama == ""){
    console.log("Nama harus diisi!");
}else if(peran == ""){
    console.log("Halo " + nama + " , Pilih peranmu untuk memulai game!");
}else if(peran == "Penyihir"){
    console.log("Selamat datang di dunia Werewolf, " + nama + "\nHalo " + peran + " " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!");
}else if(peran == "Guard"){
    console.log("Selamat datang di Dunia Werewolf, " + nama + "\nHalo " + peran + " " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.");
}else if(peran == "Werewolf"){
    console.log("Selamat datang di Dunia Werewolf, " + nama + "\nHalo "+ peran + " " + nama + ", Kamu akan memakan mangsa setiap malam!");
}

console.log("Switch case");
var tanggal = 1;
var bulan = 2;
var tahun = 2000;

var bulanKata;
switch(bulan){
    case 1 : {
        bulanKata = "Januari";
        break;
    }
    case 2 :{
        bulanKata = "Februari";
        break;
    }
    case 3 :{
        bulanKata = "Maret";
        break;
    }
    case 4 :{
        bulanKata = "April";
        break;
    }
    case 5 :{
        bulanKata = "Mei";
        break;
    }
    case 6 :{
        bulanKata = "Juni";
        break;
    }
    case 7 :{
        bulanKata = "Juli";
        break;
    }
    case 8 :{
        bulanKata = "Agustus"
        break; 
    }
    case 9 :{
        bulanKata = "September";
        break;
    }
    case 10 :{
        bulanKata = "Oktober";
        break;
    }
    case 11 :{
        bulanKata = "November";
        break;
    }
    case 12 :{
        bulanKata = "Desember";
        break;
    }
}

console.log(tanggal + " " + bulanKata + " " + tahun);
