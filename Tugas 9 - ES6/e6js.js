
const golden = () => {
   return console.log("this is golden!!");
}
golden();

const newFunction = (firstName, lastName) =>{
    return {
      firstName,
      lastName,
      fullName:() => {
        return console.log(firstName + " " + lastName);
      }
    }
}
   
  //Driver Code 
newFunction("William", "Imoh").fullName();

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const{firstName, lastName, destination, occupation} = newObject;
console.log(firstName, lastName, destination, occupation);

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west,...east]
//Driver Code
console.log(combined);

const planet = "earth"
const view = "glass"
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimad minim veniam`
 
// Driver Code
console.log(before);